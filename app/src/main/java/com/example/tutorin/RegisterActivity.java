package com.example.tutorin;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.txt_lot_explore)
    TextView txtLotExplore;
    @BindView(R.id.edNomorTelepon)
    TextInputEditText edNomorTelepon;
    @BindView(R.id.edPassword)
    TextInputEditText edPassword;
    @BindView(R.id.edKonfirmasiPassword)
    TextInputEditText edKonfirmasiPassword;
    @BindView(R.id.edNama)
    TextInputEditText edNama;
    @BindView(R.id.spin_jenis_kelamin)
    Spinner spinJenisKelamin;
    @BindView(R.id.edAlamat)
    TextInputEditText edAlamat;
    @BindView(R.id.btn_registrasi)
    Button btnRegistrasi;
    @BindView(R.id.progressLoading)
    ProgressBar progressLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        initView();
    }

    private void initView() {
        List<String> jenisKelamin = new ArrayList<>();
        jenisKelamin.add("Laki-laki");
        jenisKelamin.add("Perempuan");
        ArrayAdapter adapterJenisKelamin = new ArrayAdapter(this,android.R.layout.simple_list_item_1,jenisKelamin);
        spinJenisKelamin.setAdapter(adapterJenisKelamin);
    }
}