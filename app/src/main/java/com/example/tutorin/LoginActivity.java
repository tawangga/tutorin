package com.example.tutorin;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edNomorTelepon)
    TextInputEditText edNomorTelepon;
    @BindView(R.id.edPassword)
    TextInputEditText edPassword;
    @BindView(R.id.btn_masuk)
    Button btnMasuk;
    @BindView(R.id.progressLoading)
    ProgressBar progressLoading;
    @BindView(R.id.btn_lupa_password)
    TextView btnLupaPassword;
    @BindView(R.id.btn_registrasi)
    TextView btnDaftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.btn_registrasi)
    void onClickBtnRegistrasi(){
        startActivity(new Intent(this,RegisterActivity.class));
    }

    @OnClick(R.id.btn_masuk)
    void onClickMasuk(){
        startActivity(new Intent(this,DashboardActivity.class));
    }
}