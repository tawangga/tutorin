package com.example.tutorin.adapter;

import android.widget.ImageView;

import com.example.tutorin.R;
import com.squareup.picasso.Picasso;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class MainSliderAdapter extends SliderAdapter {

    @Override
    public int getItemCount() {
        return 3;
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        viewHolder.imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        switch (position) {
            case 0:
                Picasso.get().load(R.drawable.tutorin).into(viewHolder.imageView);
                break;
            case 1:
                Picasso.get().load(R.drawable.tutorin).into(viewHolder.imageView);
                break;
            case 2:
                Picasso.get().load(R.drawable.tutorin).into(viewHolder.imageView);
                break;
        }
    }
}