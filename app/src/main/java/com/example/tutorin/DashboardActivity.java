package com.example.tutorin;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.tutorin.adapter.MainSliderAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ss.com.bannerslider.Slider;

public class DashboardActivity extends AppCompatActivity {

    @BindView(R.id.banner_slider)
    Slider bannerSlider;
    @BindView(R.id.menu_profil)
    RelativeLayout menuProfil;
    @BindView(R.id.menu_cari_tutor)
    RelativeLayout menuCariTutor;
    @BindView(R.id.menu_notifikasi)
    RelativeLayout menuNotifikasi;
    @BindView(R.id.menu_jadwal)
    RelativeLayout menuJadwal;
    @BindView(R.id.menu_pembayaran)
    RelativeLayout menuPembayaran;
    @BindView(R.id.menu_about)
    RelativeLayout menuAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        bannerSlider.setAdapter(new MainSliderAdapter());
    }

    @OnClick(R.id.menu_about)
    void onClickAbout(){
        startActivity(new Intent(this,AboutActivity.class));
    }
}